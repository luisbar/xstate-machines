FROM node:16-alpine

# Install git and other dependencies
RUN apk add --no-cache git

# Clone the repository
RUN git clone https://github.com/statelyai/xstate-viz.git /app

# Change working directory
WORKDIR /app

# Install dependencies
RUN yarn install

# Expose the necessary port
EXPOSE 3001

# Start the application
CMD ["yarn", "start"]
