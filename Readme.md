This repo contains example of state machines for [XState](https://xstate.js.org/docs/)

## How to run?
- Run `docker-compose up -d`
- Open `http://localhost:3001/viz` in the browser
- Copy a machine from `machines folder`
- And if you want to stop the service, just run `docker-compose down`