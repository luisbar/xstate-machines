const { createMachine, interpret, assign } = require("xstate");

const retriesMachine = {
  id: "retries",
  initial: "idle",
  predictableActionArguments: true,
  context: {
    retries: null,
    makeRequest: null,
    response: null,
    error: null,
  },
  states: {
    idle: {
      on: { NEXT: "processing" },
    },
    processing: {
      entry: ["setDataForMakingRequest", "decreaseRetries"],
      invoke: [
        {
          id: "makeRequest",
          src: "makeRequest",
          onDone: {
            actions: ["setResponse"],
            target: "fulfilled",
          },
          onError: {
            actions: ["setError"],
            target: "rejected",
          },
        },
      ],
    },
    fulfilled: {
      after: {
        1: {
          actions: ["cleanError"],
          target: "settled",
        },
      },
    },
    rejected: {
      after: {
        1: [
          {
            cond: "shouldRetry",
            target: "processing",
          },
          {
            cond: "shouldNotRetry",
            target: "settled",
          },
        ],
      },
    },
    settled: {
      type: "final",
    },
  },
};

const machineOptions = {
  actions: {
    setDataForMakingRequest: assign((context, { retries, makeRequest }) => {
      if (retries) context.retries = retries;
      if (makeRequest) context.makeRequest = makeRequest;
    }),
    decreaseRetries: assign((context) => {
      context.retries = context.retries - 1;
    }),
    setResponse: assign((context, event) => {
      context.response = event.data;
    }),
    setError: assign((context, event) => {
      context.error = event.data;
    }),
    cleanError: assign((context, event) => {
      context.error = null;
    }),
  },
  guards: {
    shouldRetry: (context) => {
      return context.retries > 0;
    },
    shouldNotRetry: (context) => {
      return context.retries === 0;
    },
  },
  services: {
    makeRequest: async ({ makeRequest }) => {
      return await makeRequest();
    },
  },
};

const executeMachine = ({ retries = 3, makeRequest = () => {} }) => {
  return new Promise((resolve, reject) => {
    const retriesService = interpret(
      createMachine(retriesMachine, machineOptions)
    ).start();
    retriesService.send("NEXT", { retries, makeRequest });
    retriesService.onDone(() => {
      if (retriesService.getSnapshot().context.error)
        return reject(retriesService.getSnapshot().context.error);
      resolve(retriesService.getSnapshot().context.response);
    });
  });
};

executeMachine({
  retries: 3,
  makeRequest: () => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        reject(new Error("rejected"));
      }, 3000);
    });
  },
})
  .then((data) => console.log(data))
  .catch((error) => console.log(error));
